package start;

import java.io.Serializable;

public class StartSettings implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String teamName1;
	String teamName2;
	String pl1[];
	String pl2[];
	
	public StartSettings(String team1,String team2,String players1[],String players2[]){
		teamName1=team1;
		teamName2=team2;
		pl1=players1;
		pl2=players2;
		}

	public String getTeam1() {
		return teamName1;
	}
	public String getTeam2() {
		return teamName2;
	}
	public String[] getPlayers1() {
		return pl1;
	}
	public String[] getPlayers2() {
		return pl2;
	}
	
}

