// Stub class generated by rmic, do not edit.
// Contents subject to change without notice.

package ServerImpl;

public final class ServerImpl_Stub
    extends java.rmi.server.RemoteStub
    implements Remote.RemoteInterface, java.rmi.Remote
{
    private static final long serialVersionUID = 2;
    
    private static java.lang.reflect.Method $method_CommentRecieved_0;
    private static java.lang.reflect.Method $method_GetScore_1;
    private static java.lang.reflect.Method $method_GetStartSettings_2;
    private static java.lang.reflect.Method $method_getText_3;
    private static java.lang.reflect.Method $method_getTime_4;
    private static java.lang.reflect.Method $method_refresh_5;
    private static java.lang.reflect.Method $method_sendText_6;
    private static java.lang.reflect.Method $method_setScore_7;
    private static java.lang.reflect.Method $method_setStartSettings_8;
    private static java.lang.reflect.Method $method_setTime_9;
    
    static {
	try {
	    $method_CommentRecieved_0 = Remote.RemoteInterface.class.getMethod("CommentRecieved", new java.lang.Class[] {});
	    $method_GetScore_1 = Remote.RemoteInterface.class.getMethod("GetScore", new java.lang.Class[] {java.lang.String[].class});
	    $method_GetStartSettings_2 = Remote.RemoteInterface.class.getMethod("GetStartSettings", new java.lang.Class[] {start.StartSettings.class});
	    $method_getText_3 = Remote.RemoteInterface.class.getMethod("getText", new java.lang.Class[] {java.lang.String.class});
	    $method_getTime_4 = Remote.RemoteInterface.class.getMethod("getTime", new java.lang.Class[] {int.class});
	    $method_refresh_5 = Remote.RemoteInterface.class.getMethod("refresh", new java.lang.Class[] {});
	    $method_sendText_6 = Remote.RemoteInterface.class.getMethod("sendText", new java.lang.Class[] {int.class, java.lang.String.class});
	    $method_setScore_7 = Remote.RemoteInterface.class.getMethod("setScore", new java.lang.Class[] {});
	    $method_setStartSettings_8 = Remote.RemoteInterface.class.getMethod("setStartSettings", new java.lang.Class[] {});
	    $method_setTime_9 = Remote.RemoteInterface.class.getMethod("setTime", new java.lang.Class[] {});
	} catch (java.lang.NoSuchMethodException e) {
	    throw new java.lang.NoSuchMethodError(
		"stub class initialization failed");
	}
    }
    
    // constructors
    public ServerImpl_Stub(java.rmi.server.RemoteRef ref) {
	super(ref);
    }
    
    // methods from remote interfaces
    
    // implementation of CommentRecieved()
    public void CommentRecieved()
	throws java.rmi.RemoteException
    {
	try {
	    ref.invoke(this, $method_CommentRecieved_0, null, 8810466299251171337L);
	} catch (java.lang.RuntimeException e) {
	    throw e;
	} catch (java.rmi.RemoteException e) {
	    throw e;
	} catch (java.lang.Exception e) {
	    throw new java.rmi.UnexpectedException("undeclared checked exception", e);
	}
    }
    
    // implementation of GetScore(String[])
    public void GetScore(java.lang.String[] $param_arrayOf_String_1)
	throws java.rmi.RemoteException
    {
	try {
	    ref.invoke(this, $method_GetScore_1, new java.lang.Object[] {$param_arrayOf_String_1}, -2428968616989417508L);
	} catch (java.lang.RuntimeException e) {
	    throw e;
	} catch (java.rmi.RemoteException e) {
	    throw e;
	} catch (java.lang.Exception e) {
	    throw new java.rmi.UnexpectedException("undeclared checked exception", e);
	}
    }
    
    // implementation of GetStartSettings(StartSettings)
    public void GetStartSettings(start.StartSettings $param_StartSettings_1)
	throws java.rmi.RemoteException
    {
	try {
	    ref.invoke(this, $method_GetStartSettings_2, new java.lang.Object[] {$param_StartSettings_1}, -1316098066336383938L);
	} catch (java.lang.RuntimeException e) {
	    throw e;
	} catch (java.rmi.RemoteException e) {
	    throw e;
	} catch (java.lang.Exception e) {
	    throw new java.rmi.UnexpectedException("undeclared checked exception", e);
	}
    }
    
    // implementation of getText(String)
    public java.lang.String getText(java.lang.String $param_String_1)
	throws java.rmi.RemoteException
    {
	try {
	    Object $result = ref.invoke(this, $method_getText_3, new java.lang.Object[] {$param_String_1}, -1708672707537746981L);
	    return ((java.lang.String) $result);
	} catch (java.lang.RuntimeException e) {
	    throw e;
	} catch (java.rmi.RemoteException e) {
	    throw e;
	} catch (java.lang.Exception e) {
	    throw new java.rmi.UnexpectedException("undeclared checked exception", e);
	}
    }
    
    // implementation of getTime(int)
    public void getTime(int $param_int_1)
	throws java.rmi.RemoteException
    {
	try {
	    ref.invoke(this, $method_getTime_4, new java.lang.Object[] {new java.lang.Integer($param_int_1)}, -4695656223522398155L);
	} catch (java.lang.RuntimeException e) {
	    throw e;
	} catch (java.rmi.RemoteException e) {
	    throw e;
	} catch (java.lang.Exception e) {
	    throw new java.rmi.UnexpectedException("undeclared checked exception", e);
	}
    }
    
    // implementation of refresh()
    public boolean refresh()
	throws java.rmi.RemoteException
    {
	try {
	    Object $result = ref.invoke(this, $method_refresh_5, null, 6099747806094767484L);
	    return ((java.lang.Boolean) $result).booleanValue();
	} catch (java.lang.RuntimeException e) {
	    throw e;
	} catch (java.rmi.RemoteException e) {
	    throw e;
	} catch (java.lang.Exception e) {
	    throw new java.rmi.UnexpectedException("undeclared checked exception", e);
	}
    }
    
    // implementation of sendText(int, String)
    public void sendText(int $param_int_1, java.lang.String $param_String_2)
	throws java.rmi.RemoteException
    {
	try {
	    ref.invoke(this, $method_sendText_6, new java.lang.Object[] {new java.lang.Integer($param_int_1), $param_String_2}, 3605308272508383737L);
	} catch (java.lang.RuntimeException e) {
	    throw e;
	} catch (java.rmi.RemoteException e) {
	    throw e;
	} catch (java.lang.Exception e) {
	    throw new java.rmi.UnexpectedException("undeclared checked exception", e);
	}
    }
    
    // implementation of setScore()
    public java.lang.String[] setScore()
	throws java.rmi.RemoteException
    {
	try {
	    Object $result = ref.invoke(this, $method_setScore_7, null, 9175987376722299658L);
	    return ((java.lang.String[]) $result);
	} catch (java.lang.RuntimeException e) {
	    throw e;
	} catch (java.rmi.RemoteException e) {
	    throw e;
	} catch (java.lang.Exception e) {
	    throw new java.rmi.UnexpectedException("undeclared checked exception", e);
	}
    }
    
    // implementation of setStartSettings()
    public start.StartSettings setStartSettings()
	throws java.rmi.RemoteException
    {
	try {
	    Object $result = ref.invoke(this, $method_setStartSettings_8, null, 8694713608038928052L);
	    return ((start.StartSettings) $result);
	} catch (java.lang.RuntimeException e) {
	    throw e;
	} catch (java.rmi.RemoteException e) {
	    throw e;
	} catch (java.lang.Exception e) {
	    throw new java.rmi.UnexpectedException("undeclared checked exception", e);
	}
    }
    
    // implementation of setTime()
    public int setTime()
	throws java.rmi.RemoteException
    {
	try {
	    Object $result = ref.invoke(this, $method_setTime_9, null, 1952702839532966554L);
	    return ((java.lang.Integer) $result).intValue();
	} catch (java.lang.RuntimeException e) {
	    throw e;
	} catch (java.rmi.RemoteException e) {
	    throw e;
	} catch (java.lang.Exception e) {
	    throw new java.rmi.UnexpectedException("undeclared checked exception", e);
	}
    }
}
