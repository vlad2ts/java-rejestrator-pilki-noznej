package Gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import start.StartSettings;

public class GuiKlient extends JFrame implements ActionListener {

	 /**
	 * 
	 */
	protected String message="Lets get ready to rumble!!!";
	private static final long serialVersionUID = 1L;
	
	final JTextPane field = new JTextPane();
	JTextPane area;
	JButton comment;
	JButton start;
	JButton add;
	JButton insert;
	 Stopper stpWatch= new Stopper();
	JComboBox pl1;
	JComboBox pl2;
	JButton rmv;
	JComboBox cb1;
	JComboBox cb2;
	JComboBox score1;
	JComboBox score2;
	int combobox = 0;
	String play1[];
	String play2[];
	String scoreSend[]= new String[2];
	public boolean NewMsg; 
	StartSettings setts;
	
	GuiKlient(){
		 
			 
		 JFrame frame = new JFrame("klient");
		 frame.setSize(500,300);
		 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		 JPanel panel = new JPanel();
		 JPanel inpanel = new JPanel();
		 JPanel flow1 = new JPanel();
		 JPanel flow2 = new JPanel();
		 JPanel flow3 = new JPanel();
		 JPanel mainPanel = new JPanel();
		 
		
	        
		 String languages[]={"Zespoly","Barcelona","Bayern"}; 
		 String Actions[]= {"Akcje","Zolta","Czerwona","Gol","Rzut rozny","Faul","Rzut karny","Zmiana","Offside"};		 
		 String Players1[]= {"Grzacze 1","Messi","Suarez","Ter Stegen","Coutinho","Pique","Rakitic","Busquets","Dembele","Rafinha","Umtiti","Vermaelen"};
		 String Players2[]= {"Gracze 2","Neuer","Lewandowski","Hummels","Ribery","Robben","James","Muller","Kimmich","Alaba","Boateng","Goretzka"};
		 String score[]= {"0","1","2","3","4","5","6","7","8","9","10"};
		     cb1=new JComboBox(languages);
		     cb2=new JComboBox(Actions);
		     pl1=new JComboBox(Players1);
		     pl2=new JComboBox(Players2);
		     score1 = new JComboBox(score);
		     score2 = new JComboBox(score);
	        rmv = new JButton("Remove");
	 
		 insert = new JButton("Insert");
		 comment=new JButton("Comment");
		 start=new JButton("Start");
		  add = new JButton("Add");
		 
		 JButton setScore =new JButton("Score");  
		  pl1.setEditable(true);
		  pl2.setEditable(true);
		  cb1.setEditable(true);
		 area = new JTextPane();
		 JScrollPane scrollPane = new JScrollPane(area);
		 comment.addActionListener(this);
		 start.addActionListener(this);
		 score1.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) {
	             combobox= 5;
	             scoreSend[0]=(String) score1.getSelectedItem();
	             System.out.println(combobox);
	         }});
		 
		 score2.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) {
	             combobox= 5;
	             scoreSend[1]=(String) score2.getSelectedItem();
	             System.out.println(combobox);
	         }});
		 
		 cb1.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) {
	             combobox= 3;
	             System.out.println(combobox);
	         }});
		 cb2.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) {
	             combobox= 6;
	             System.out.println(combobox);
	         }});
		 pl1.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) {
             combobox= 1;
             System.out.println(combobox);
         }});
		 pl2.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) {
	             combobox= 2;
	             System.out.println(combobox);
	         }});
		 insert.addActionListener(this);
		 add.addActionListener(this);
		 rmv.addActionListener(this);
		 flow1.add(cb1);flow1.add(pl1);flow1.add(comment);
		 flow2.add(cb2);flow2.add(pl2);flow2.add(start);
		 flow1.setLayout(new GridLayout(1,4,5,5));
		 flow2.setLayout(new GridLayout(1,4,5,5));
		 flow3.add(add); flow3.add(score1); flow3.add(score2);flow3.add(rmv);flow3.add(insert);
		 flow3.setLayout(new GridLayout(1,5,10,10));
		 panel.setLayout(new GridLayout(4,1,3,3));
			
         panel.add(field);
		 panel.add(flow1);
		 panel.add(flow2);
		 panel.add(flow3);
		 panel.setVisible(true);
		 
		 
		 frame.add(panel);
	//	 frame.add(scrollPane);
		 
		// frame.setLayout(new GridLayout(2,1));
		 frame.setVisible(true);
				
	 }

	private ActionListener createSearchActionListener() {
		
		return null;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == comment) {
		
		String str = field.getText();
		message= str;
		NewMsg=true;
		System.out.println(sendMessage());
		
		}
		
		else if(e.getSource()==start) {
			
			if(!stpWatch.started()) {
				stpWatch.start();
				System.out.println("Stopwatch started");
				start.setText("Stop");
				
			} else if(stpWatch.started()){
				stpWatch.stop();
				System.out.println("Stop Time:"+stpWatch.getTotalTime());
				
			}
						
		}else if (e.getSource()==insert) {
			String Item = ""; 
			if(combobox==1) {
				Item= pl1.getSelectedItem().toString();
			}else if (combobox==2) {
				Item= pl2.getSelectedItem().toString();
			}else if (combobox==6) {
				Item= cb2.getSelectedItem().toString();
			}
			try {
			      Document doc = field.getDocument();
			      doc.insertString(doc.getLength()," "+Item, null);
			   } catch(BadLocationException exc) {
			      exc.printStackTrace();
			   }
			
		}
		
		else if(e.getSource()==add) {
			String name;
			if(combobox==1) {
			name = pl1.getSelectedItem().toString();
			pl1.addItem(name);
			} else if (combobox==2) {
		     name = pl2.getSelectedItem().toString();
			pl2.addItem(name);
			
			}	

			else if(combobox==3) {
				
				name = cb1.getSelectedItem().toString();
				cb1.addItem(name);
			}
		}
		else if(e.getSource()==rmv) {
			if(combobox==1) {
				pl1.removeItemAt(pl1.getSelectedIndex());
				
			} else if (combobox==2) {
				pl2.removeItemAt(pl2.getSelectedIndex());

			}else if(combobox==3) {
				
				cb1.removeItemAt(pl2.getSelectedIndex());
			}
			     		
		
		}
		
		
	}
	
	
		
	public String sendMessage() {
		 
		return message;
			 
	}
	public boolean NewComment() {
		return NewMsg;
	}
	
	public void CommentSent() {
		NewMsg=false;
	} 
	
	public int getTime() {
		
		return stpWatch.getTotalTime();
	}
	public int getCurrentTime() {
		return stpWatch.getCurrentTime();
	}
	public StartSettings setParameters() {
		String tm1,tm2;
		
		tm1=(String)cb1.getItemAt(1);
		tm2=(String)cb1.getItemAt(2);
		int size1 = pl1.getItemCount();
		play1=new String[size1];
		
		for (int i = 0; i < size1; i++) {
		  
		  play1[i] = (String) pl1.getItemAt(i);
		  		  
		}
		int size2 = pl2.getItemCount();  
		play2=new String[size2];
		for (int i = 0; i < size2; i++) {
		  
		  play2[i] = (String) pl2.getItemAt(i);
		}
		setts=new StartSettings(tm1,tm2,play1,play2);
		System.out.println("parameters sent");
		
		return setts;
	}
	public boolean TranslationStarted() {
		return stpWatch.started();
	}
	public String[] sendScore() {
	
		return scoreSend;
	}
	 
}
	

