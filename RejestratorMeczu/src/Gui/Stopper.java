package Gui;

import java.util.concurrent.TimeUnit;

public class Stopper {

	 private long startTime = 0;
	  private long endTime   = 0;
	  private boolean start = false;
	  private long min=0;
	  private long current,currentTime,timemin;

	  public void start(){
	    this.startTime = System.currentTimeMillis();
	    start=true;
	  }
	  public boolean started() {
		  return start;
	  }

	  public void stop() {
	    this.endTime   = System.currentTimeMillis();  
	    start=false;
	  }

	  public long getStartTime() {
	    return this.startTime;
	  }

	  public long getEndTime() {
	    return this.endTime;
	  }

	  public int getTotalTime() {
	  long millis = this.endTime - this.startTime;
	   min = 1+TimeUnit.MILLISECONDS.toMinutes(millis);
	   int minInt= (int)min;
	   return minInt;
	  }
	  public int getCurrentTime() {
		  current = System.currentTimeMillis();		 
		  currentTime= current-startTime;
		  timemin=1+TimeUnit.MILLISECONDS.toMinutes(currentTime);
		  int intTime=(int)timemin;
		  return intTime;
	  }
	
}
