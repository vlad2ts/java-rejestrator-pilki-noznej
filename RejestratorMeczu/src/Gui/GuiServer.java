package Gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import start.StartSettings;

public class GuiServer {
	protected String message;
	static JTextPane area;
	JTextArea team1;
	JTextArea team2;
	StartSettings params;
	JTextArea czas;
	int time;
	static String timetxt;
	JTextArea score1;
	JTextArea score2;
	GuiServer(){
		
		 
		 JFrame frame = new JFrame("Server");
		 frame.setSize(500,600);
		 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 JPanel panel = new JPanel();
		 
		 JPanel flow1 = new JPanel();
		 JPanel flow2 = new JPanel();
		
		  team1 =new JTextArea("");
		  team2 =new JTextArea("");
		  score1 =new JTextArea("3");  
		  score2 =new JTextArea("0");
		 czas = new JTextArea("76");
		 JButton refresh=new JButton("Refresh");
		 area = new JTextPane();
		 area.setEditable(false);
		 JScrollPane scrollPane = new JScrollPane(area);
		 
		 flow1.add(team1);flow1.add(score1);flow1.add(score2);flow1.add(team2);
		 flow2.add(czas);
		 flow1.setLayout(new GridLayout(1,4,5,5));
		 flow2.setLayout(new GridLayout(1,3,5,5));
		
		 panel.setLayout(new GridLayout(2,1,10,10));
		
		 panel.add(flow1);
		 panel.add(flow2);
		
		 panel.setVisible(true);
		 
		 frame.add(panel);
		 frame.add(scrollPane);
		 
		 frame.setLayout(new GridLayout(2,1));
		 frame.setVisible(true);
		
	}
	
	public static  void getMessage(String txt) {
		System.out.println(txt);
		
		try {
		      Document doc = area.getDocument();
		      doc.insertString(doc.getLength(),timetxt+" "+ txt+"\n", null);
		   } catch(BadLocationException exc) {
		      exc.printStackTrace();
		   }
		
		
	}
	
	public void SetTeams(StartSettings sets) throws BadLocationException {
		params=sets;
		team1.setText("");
		team2.setText("");
		area.setText("");
		team1.insert(params.getTeam1(), 0);
		team2.insert(params.getTeam2(), 0);
		String arr1[]=params.getPlayers1();
		String arr2[]=params.getPlayers2();
		int size1 = (arr1).length;
		int size2 = (arr2).length;
		String txt;
		Document doc = area.getDocument();
		for( int i = 0; i < size1 ; i++) {
			txt= arr1[i];
		
	      doc.insertString(doc.getLength(), txt+"\n", null);	
		}
		doc.insertString(doc.getLength(), "\n"+"\n", null);
		for( int i = 0; i < size2 ; i++) {
			txt= arr2[i];		
	      doc.insertString(doc.getLength(), txt+"\n", null);
	
		}
		System.out.println("parameters init");
	}
	public void GetTime(int tm) {
		time=tm;
		timetxt=String.valueOf(time);
		czas.setText(timetxt);
	}
	public void GetScore(String score[]) {
		
		score1.setText(score[0]);
		score2.setText(score[1]);
		
	}

}
