package Remote;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import start.StartSettings;

public interface RemoteInterface extends Remote {
	
	public void sendText(int time,String Text) throws RemoteException ;
	public String getText(String id) throws RemoteException;
	public boolean refresh () throws RemoteException;
	public void CommentRecieved() throws RemoteException;
	public void GetStartSettings(StartSettings param)throws RemoteException;
	public void GetScore(String sc[]) throws RemoteException;
	public StartSettings setStartSettings()throws RemoteException;
	public String[] setScore() throws RemoteException;
	public void getTime(int Time)throws RemoteException;
	public int setTime()throws RemoteException;

 
}

 

